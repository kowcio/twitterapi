package twitterapi.rest.controller;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import twitter4j.*;
import twitterapi.twitter.TwitterStreamBean;
import twitterapi.twitter.TwitterStreamBuilderUtil;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/rest")
public class RestTwitterDataController {

    Logger log = LoggerFactory.getLogger(getClass());

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public Map<String, Map<String, Integer>> hello() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Integer> users = TwitterStreamBean.getUsers();
        Map<String, Map<String, Integer>> singletonMap = Collections.singletonMap("users", users);
        String prettyJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(singletonMap);
        log.info(prettyJson);
        return singletonMap;
    }

    @ResponseBody
    @RequestMapping(value = "/log", method = RequestMethod.GET)
    public Map<String, Map<String, Integer>> getTwitterBotActivity() throws JsonProcessingException {
        TwitterFactory tf = TwitterStreamBuilderUtil.getTwitterFactory();
        Twitter tw = tf.getInstance();

        Query q = new Query("burger");
        //        Query.ResultType rt =  q.getResultType();
        try {
            QueryResult search = tw.search().search(q);
            List<Status> tweets = search.getTweets();

            //list of tweets, add something to them !
            for (Status s : tweets){
//                /https://forum.processing.org/one/topic/twitter4j-reply-to-tweet.html
                Status myAutoResponseTweet = (Status) new StatusAdapter();




            }
        } catch (TwitterException e) {
            e.printStackTrace();
        }


        //get stats from DB
        return null;
    }

    @RequestMapping(value = "/healthcheck", method = RequestMethod.GET)
    public boolean healthcheck() {
        try {
            TwitterStreamBuilderUtil.getStream().getOAuthAccessToken();

        } catch (TwitterException e) {
            log.info("Twitter os down or unavailable.");
            return false;
        }
        return TwitterStreamBean.isTwitterOnline();
    }

}
