package twitterapi.service.twitter;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import twitter4j.FilterQuery;
import twitter4j.TwitterStream;
import twitterapi.constants.TwitterConstants;
import twitterapi.twitter.TwitterStatusListener;
import twitterapi.twitter.TwitterStreamBuilderUtil;

/**
 * Twitter service launching while application is up. Counting tweets with filtered word.
 * @author PK
 *
 */
@Service
public class StreamReaderService {

    Logger                      logger = LoggerFactory.getLogger(getClass());
    public static TwitterStream stream = TwitterStreamBuilderUtil.getStream();

    @PostConstruct
    public void readTwitterFeed() {
        TwitterStatusListener listener = new TwitterStatusListener();

        FilterQuery qry = new FilterQuery();
        String[] keywords = { TwitterConstants.FILTER_STREAM_BY_JAVA };

        qry.track(keywords);

        stream.addListener(listener);
        stream.filter(qry);
    }
    
    public void killTweeterStream(){
    	stream.shutdown();// shutdown internal stream consuming thread
    	stream.cleanUp();// Shuts down internal dispatcher thread shared by all TwitterStream instances.
    }
    
}
