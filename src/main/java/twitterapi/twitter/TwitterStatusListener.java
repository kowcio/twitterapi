package twitterapi.twitter;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.*;

/**
 * Twitter listener logic that counts the tweets filtered with word in
 * {@link twitterapi.constants.TwitterConstants}
 * 
 * @author PK
 *
 */
public class TwitterStatusListener implements StatusListener {
    Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void onException(Exception e) {
        TwitterStreamBean.setTwitterOnline(false);
        logger.info("Exception occured: {}", e.getMessage());
        e.printStackTrace();
    }

    @Override
    public void onTrackLimitationNotice(int n) {
        logger.info("Track limitation notice for " + n);
    }

    @Override
    public void onStatus(Status tweet) {
        logger.info("Got twit: {} ", tweet.getText());
        TwitterStreamBean.setTwitterOnline(true);
        String username = tweet.getUser().getScreenName();
        Map<String, Integer> users = TwitterStreamBean.getUsers();

        if (users.get(username) != null) {
            int value = users.get(username);
            users.put(username, value + 1);
        } else {
            users.put(username, 1);
        }

    }

    @Override
    public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onScrubGeo(long userId, long upToStatusId) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onStallWarning(StallWarning warning) {
        // TODO Auto-generated method stub

    }

}
