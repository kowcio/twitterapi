package twitterapi.twitter;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Class exposing the variables of rest services for checking data gathered from
 * twitter.
 * 
 * @author pk
 *
 */
@Component
public class TwitterStreamBean {

    private static Map<String, Integer> users    = new HashMap<String, Integer>();
    private static boolean              isTwitterOnline;

    private static TwitterStreamBean    instance = null;

    public static TwitterStreamBean getInstance() {
        if (instance == null) {
            instance = new TwitterStreamBean();
        }
        return instance;
    }

    public static Map<String, Integer> getUsers() {
        return users;
    }

    public static boolean isTwitterOnline() {
        return isTwitterOnline;
    }

    public static void setTwitterOnline(boolean isTwitterOnline) {
        TwitterStreamBean.isTwitterOnline = isTwitterOnline;
    }
}
