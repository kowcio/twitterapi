package twitterapi.twitter;

import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;
import twitterapi.constants.TwitterConstants;

public class TwitterStreamBuilderUtil {

	public static TwitterStream getStream() {
		ConfigurationBuilder cb = getTwitterConfigurationBuilder();
		return new TwitterStreamFactory(cb.build()).getInstance();
	}

	public static TwitterFactory getTwitterFactory () {
		ConfigurationBuilder cb = getTwitterConfigurationBuilder();
		return new TwitterFactory (cb.build());
	}


	private static ConfigurationBuilder getTwitterConfigurationBuilder() {
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true);
		cb.setOAuthConsumerKey(TwitterConstants.consumerkey);
		cb.setOAuthConsumerSecret(TwitterConstants.consumerSecret);
		cb.setOAuthAccessToken(TwitterConstants.accessToken);
		cb.setOAuthAccessTokenSecret(TwitterConstants.accessTokenSecret);
		return cb;
	}
}