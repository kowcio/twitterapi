package allinone.test;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.fasterxml.jackson.core.JsonProcessingException;

import twitterapi.Application;
import twitterapi.rest.controller.RestTwitterDataController;
import twitterapi.service.twitter.StreamReaderService;
import twitterapi.twitter.TwitterStreamBean;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class ServiceTest {

	private static final int TWITTER_TEST_TIMEOUT = 10000;
	private static Logger log = LoggerFactory.getLogger(ServiceTest.class);
	StreamReaderService streamReaderService = new StreamReaderService();

	/**
	 * Check if twitter service is on. Arbitrary set timeout.
	 * API rates - test can fail. Test turned off due to failing because of multiple logins issues.
	 * https://dev.twitter.com/rest/public/rate-limiting
	 * @throws InterruptedException
	 * @throws JsonProcessingException
	 */
	@Test
	@Ignore
	public void twitterRestControllerServiceTest() throws InterruptedException, JsonProcessingException {

		streamReaderService.readTwitterFeed();

		
		
//		StreamReaderService streamReaderService = new StreamReaderService();
		streamReaderService.readTwitterFeed();
		boolean isTwitterMapEmpty = TwitterStreamBean.getUsers().isEmpty();
		log.info("Is Twitter map empty = " + isTwitterMapEmpty);
		Assert.assertTrue(isTwitterMapEmpty);
		
		log.info("Sleeping, waiting for tweets.");
		Thread.sleep(TWITTER_TEST_TIMEOUT);
		Assert.assertTrue(TwitterStreamBean.isTwitterOnline());
		RestTwitterDataController restController = new RestTwitterDataController();
		Assert.assertTrue(restController.healthcheck());
		int mapString = restController.hello().size();
		log.info("Map from controller = " + mapString);
		Assert.assertTrue(mapString > 0);
		streamReaderService.killTweeterStream();

	}

	@After
	public void afterTest() {
		log.info("Kiling twitter stream.");
		streamReaderService.killTweeterStream();

	}

}
