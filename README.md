# Twitter API test project. #

 CI
##https://drone.io/bitbucket.org/kowcio/twitterapi##

Build project

*mvn clean install*
Copy the jar to the pom level directory

Build docker image  with dependencies : 
*mvn clean package docker:build*

Docker image : https://hub.docker.com/r/kowcio/twitterapi/

*mvn push kowcio/twitterapi:latest*

*docker run --rm -it -p 8080:8080 twitterapi*

after running check Kitematic running image in settings -> ports and access the API as
<ip>:8080/rest/users
<ip>:8080/rest/healthcheck

i.ex http://192.168.99.100:8080/rest/healthcheck

Application will allow access the map of users that tweeted the word "java" and a healtcheck service to tell us if we can access twitter.


**Trouleshooting :**  
Tests can fail due to to many logging attempts.

Repo name upper case bug.
Read bottom.
https://github.com/spotify/docker-maven-plugin
Check ip and ports in docker toolbox ports for exposed ones,
add mapping for port in the docker VM.

Last point
http://stackoverflow.com/questions/33814696/how-to-connect-to-a-docker-container-from-outside-the-host-same-network-windo

Remember to tag the image when pushing to repo.
*docker tag <image-id> <repository>
docker push <repository>*